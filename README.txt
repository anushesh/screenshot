# SCreen shot Rest Api using selienum

# This is an springBoot backend Application which gives the screent shot of a web page  if we give url of web page and the browser in which thats page is opened for screen shot
# The parent screenshot folder has src and pom file , it needs to import as maven project in eclipse 
# build project using pom
# This projects uses inbuilt tomcat server which runs on 9090 port 
# To run the application  goto ScreenShotApplication in src/main/java and run as Java Application
# it can also deploy on external tomcat server as war file but for this tomcat configuration needs to done in pom.xml
# test case can be run by  ScreenShotControllerTest file run as Junit placed in src/test/java 
# once application is running test the API using postman , following is the details of REST API
#first the screen shot is saved in d://test1.png location then is it read and sent back to API as .png image file  response
# the geckodriver.exe file needs to put in this directory ->D:/FirefoxExe/geckodriver.exe the exe is in FirefoxExe folder 
# URL ->localhost:9090/screenshot
method ->POST
Request Body 

{
   "url":"https://en.wikipedia.org/wiki/Java_(programming_language)",
	"browser":"Firefox"
   
}
 currently firefox driver is used in code base

Response 

image file as .png extensions

