package com.ajackus.screenshot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jsonb.JsonbAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * The Class ScreenShotApplication, startup class for ScreenShot functionality
 * Application
 */
@SpringBootApplication
@ComponentScan({ "com.ajackus.screenshot" })
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, JsonbAutoConfiguration.class })
public class ScreenShotApplication extends SpringBootServletInitializer implements WebMvcConfigurer {

	/**
	 * The main method, the startup method for spring boot application
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ScreenShotApplication.class, args);
	}

	/**
	 * Configure, method to configure application sources
	 *
	 * @param application the application
	 * @return the spring application builder
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ScreenShotApplication.class);
	}
}
