package com.ajackus.screenshot.bo.request;

/**
 * The Class ScreenShot, for holding the request information
 */
public class ScreenShot {

	private String url;
	private String browser;

	public ScreenShot() {
	}

	public ScreenShot(String url, String browser) {
		this.url=url;
		this.browser=browser;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}
	
	@Override
	public String toString() {
		return "ScreenShot [url=" + getUrl() + ", browser=" + getBrowser() + "]";
	}

}
