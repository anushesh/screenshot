package com.ajackus.screenshot.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * The Class ScreenShotConfig, for storing various application
 * configurations
 */
@Configuration
@PropertySource(ignoreResourceNotFound = true, value = "classpath:screenshot.properties")
public class ScreenShotConfig {

	@Value("${webdriver.driver.firefox}")
	private String browser;

	@Value("${browser.driver.firefox}")
	private String driver;
	public String getBrowser() {
		return browser;
	}
	public String getDriver() {
		return driver;
	}

	
}
