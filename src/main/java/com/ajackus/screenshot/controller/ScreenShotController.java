package com.ajackus.screenshot.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ajackus.screenshot.bo.request.ScreenShot;
import com.ajackus.screenshot.service.ScreenShotService;
import com.ajackus.screenshot.util.KeyConstant;

/**
 * The Class ScreenShotController
 */
@RestController
public class ScreenShotController {

	private static final Logger LOGGER = LogManager.getLogger(ScreenShotController.class);

	@Resource
	ScreenShotService screenShotService;

	/**
	 * getScreenShot, end point method for screen shot using selenium
	 *
	 * @param screenshotRequest the screen shot request object
	 * @return the response entity for returning the json response
	 */
	@PostMapping(value = "/screenshot", produces = MediaType.IMAGE_PNG_VALUE)
	public ResponseEntity<InputStreamResource> getScreenShot(@RequestBody ScreenShot screenshotRequest) {
		LOGGER.info("url >>" + screenshotRequest.getUrl() + "Browser >>" + screenshotRequest.getBrowser());
		HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;

		InputStreamResource resource = null;

		if (Objects.isNull(screenshotRequest)) {
			return ResponseEntity.status(status).body(resource);
		}
		Map<String, Object> response = new HashMap<>();
		try {
			response = screenShotService.getScreenshot(screenshotRequest);
            
			//File file = new File((String) response.get("filePath"));
			File file = new File(KeyConstant.FILEPATH);
			
			resource = new InputStreamResource(new FileInputStream(file));

			return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
					.header("Content-Disposition", "attachment; filename=" + "testing.png").body(resource);
		} catch (IOException e) {
			status = HttpStatus.NOT_FOUND;
			response.put(KeyConstant.ERROR_CODE, status.value());
			response.put(KeyConstant.ERROR_DESCRIPTION, e.getMessage());
			return ResponseEntity.status(status).body(resource);
		}

	}
}
