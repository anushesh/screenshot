package com.ajackus.screenshot.service;

import java.io.IOException;
import java.util.Map;

import com.ajackus.screenshot.bo.request.ScreenShot;



/**
 * The Interface ScreenShotService, the base interface for screen shot
 * service
 */
public interface ScreenShotService {

	Map<String,Object> getScreenshot(ScreenShot screenshotRequest) throws IOException;
}