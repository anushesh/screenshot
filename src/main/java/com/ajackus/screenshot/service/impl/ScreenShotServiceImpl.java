package com.ajackus.screenshot.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import javax.imageio.ImageIO;
import org.springframework.stereotype.Service;

import com.ajackus.screenshot.bo.request.ScreenShot;
import com.ajackus.screenshot.config.ScreenShotConfig;
import com.ajackus.screenshot.service.ScreenShotService;
import com.ajackus.screenshot.util.KeyConstant;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

/**
 * The Class ScreenShotServiceImpl, an implementation of the Screen shot service
 */
@Service
public class ScreenShotServiceImpl implements ScreenShotService {

	private static final Logger LOGGER = LogManager.getLogger(ScreenShotService.class);

	/** The ScreenShotConfig config, to store application specific configurations */
	@Resource
	private ScreenShotConfig screenShotConfig;

	/**
	 * getScreenshot, method to actually call the selenium apis
	 *
	 * @param screenshotRequest having url and browers whose screen shot need to be
	 *                          taken
	 * @return the map, response
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public Map<String, Object> getScreenshot(ScreenShot screenshotRequest) throws IOException {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		if (Objects.isNull(screenshotRequest) || Objects.isNull(screenshotRequest.getUrl())
				|| Objects.isNull(screenshotRequest.getBrowser())) {
			return Collections.EMPTY_MAP;
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		//System.setProperty("webdriver.gecko.driver", "D:/FirefoxExe/geckodriver.exe");
		System.setProperty(screenShotConfig.getBrowser(), screenShotConfig.getDriver());

		// WebDriver driver = new FirefoxDriver();
		WebDriver driver = null;

		if (screenshotRequest.getBrowser().equalsIgnoreCase(KeyConstant.FIREFOX)) {
			driver = new FirefoxDriver();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(screenshotRequest.getUrl());

		// Convert web driver object to TakeScreenshot

		// TakesScreenshot scrShot = ((TakesScreenshot) driver);
		Screenshot scrShot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
				.takeScreenshot(driver);

		// Call getScreenshotAs method to create image file

		// File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		File SrcFile = new File(KeyConstant.FILEPATH);

		ImageIO.write(scrShot.getImage(), "PNG", SrcFile);

		// Move image file to new destination

		// File DestFile = new File("d://test1.png");

		// Copy file at destination

		// FileUtils.copyFile(SrcFile, DestFile);

		driver.close();
		// driver.quit();

		LOGGER.info("screen shot exiting method ");

		return (Map<String, Object>) dataMap.put("filePath", SrcFile);
	}

}
