package com.ajackus.screenshot.util;

/**
 * The Class KeyConstant, to represent keys of JSON request/ response
 */
public final class KeyConstant {
	
	/** The Constant for different browser. */
	public static final String FIREFOX = "Firefox";

	/** The Constant ERROR_CODE. */
	public static final String ERROR_CODE = "errorCode";

	/** The Constant ERROR_DESCRIPTION. */
	public static final String ERROR_DESCRIPTION = "errorDescription";

	/** The Constant DATA. */
	public static final String DATA = "data";
	
	/** The Constant FILEPATH. */
	public static final String FILEPATH = "d://temp.png";

}
