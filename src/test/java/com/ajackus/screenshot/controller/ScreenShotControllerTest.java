package com.ajackus.screenshot.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import javax.annotation.Resource;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.ajackus.screenshot.bo.request.ScreenShot;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
class ScreenShotControllerTest {

	@Resource
	protected MockMvc mockMvc;

	@Test
	void testNotNull() {
		assertNotNull(mockMvc);
	}

	@Test
	void testNoRequest() throws Exception {
		mockMvc.perform(post("/screenshot")).andExpect(MockMvcResultMatchers.status().is(HttpStatus.BAD_REQUEST.value()))
				.andDo(MockMvcResultHandlers.print());
	}

	
	
	@Test
	void testRequestWithUrl() throws Exception {
		String url = "https://en.wikipedia.org/wiki/Java_(programming_language)";
		String browser = "Firefox";
		ScreenShot screenShot = new ScreenShot(url, browser);
		String request = new ObjectMapper().writeValueAsString(screenShot);
		mockMvc.perform(post("/screenshot").contentType(MediaType.APPLICATION_JSON_VALUE).content(request))
				.andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value()))
				.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	void testRequestUrl() throws Exception {
		String url = "https://www.google.com/";
		String browser = "Firefox";
		ScreenShot screenShot = new ScreenShot(url,browser);
		String request = new ObjectMapper().writeValueAsString(screenShot);
		mockMvc.perform(post("/screenshot").contentType(MediaType.APPLICATION_JSON_VALUE).content(request))
				.andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value()))
				.andDo(MockMvcResultHandlers.print());
	}

}
